export default function Footer() {
  return (
    <div className="d-flex flex-wrap justify-content-around">
      <a target="_blank" href="https://gitlab.com/vmaxqb/fcc-next/" className="px-5">
        <i className="fab fa-git-square"></i> Repo of this website
      </a>
    </div>
  )
}

