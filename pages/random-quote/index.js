import React from 'react'
import Head from 'next/head'
import Layout from '../../components/layout'
import styles from '../../components/random-quote.module.css'
import Footer from '../../components/footer';

const quotes = [
  {
    text: 'If your life can change once, your life can change again.',
    author: 'Sanae'
  },
  {
    text: 'Fools who don’t respect the past are likely to repeat it.',
    author: 'Nico Robin'
  },
  {
    text: 'Even If you’ve only got a 1% chance of winning, but you convince yourself you’re gonna lose, that 1% becomes 0%.',
    author: 'Lina Inverse'
  },
  {
    text: 'The world isn’t perfect. But it’s there for us, doing the best it can….that’s what makes it so damn beautiful.',
    author: 'Roy Mustang'
  },
  {
    text: 'Those who get fooled are partially at fault',
    author: 'Senjougahara Hitagi'
  },
  {
    text: 'Fake people have an image to maintain. Real people just don’t care.',
    author: 'Hachiman Hikigaya'
  },
  {
    text: 'The world is not beautiful. Therefore it is.',
    author: 'Kino'
  },
  {
    text: 'Perhaps living without sin is a sin in itself',
    author: 'Furude Rika'
  },
  {
    text: 'If you can never get angry at anything, that probably means you have nothing that you like, either',
    author: 'Eru Chitanda'
  },
  {
    text: 'If you use your wish to grant someone else\'s, you should be careful you know exactly why you are doing it. Are you really doing it for someone else\'s benefit? Or the opposite, you\'re doing it for that person\'s everlasting gratitude',
    author: 'Tomoe Mami'
  },
  {
    text: 'People who neglect to make efforts or who don\'t take any actions at all are always the ones who dream that someday they will suddenly become wildly successful',
    author: 'Misaki Nakahara'
  },
  {
    text: 'Humans are pitiful beings, walking forward their backs to the future gazing at the past. This is why they fail to notice the simplest pitfalls and stumble tragically and comically',
    author: 'Frederica Bernkastel'
  },
  {
    text: 'There\'s only one difference between heroes and mad men. It\'s whether they win or lose',
    author: 'Lambdadelta'
  }
];

class RandomQuote extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      curQuote: ''
    }
  }

  componentDidMount = () => {
    this.setNewQuote()
  }

  setNewQuote = () => {
    this.setState({
      curQuote: quotes[Math.floor(Math.random() * quotes.length)]
    })
  }

  render() {
    const twitterUrl = "https://twitter.com/intent/tweet?text=" + encodeURIComponent(this.state.curQuote.text) + "%0A-%20" + encodeURIComponent(this.state.curQuote.author)

    return (
      <Layout>
        <Head>
          <title>Random quote</title>
        </Head>
        <div className={styles.app}>
          <div className={styles.appContainer}>
            <div className="text-center">
              <h1>Random quote</h1>
            </div>
            <div id="quote-box" className={styles.quoteBox}>
              <p id="text" className={styles.text}>{this.state.curQuote.text}</p>
              <p id="author" className={styles.author}>- {this.state.curQuote.author}</p>
              <div className="d-flex mt-4">
                <div id="tweet-quote">
                  <a href={twitterUrl}>
                    <i className={"fab fa-twitter-square " + styles.smButton }></i>
                  </a>
                </div>
                <div className="ml-auto">
                  <button id="new-quote" className="btn btn-lg btn-primary" onClick={this.setNewQuote}>New quote</button>
                </div>
              </div>
            </div>
          </div>
          
          <Footer></Footer>
        </div>
      </Layout>
    );
  }
}

export default RandomQuote;
